import { ProvAppPage } from './app.po';

describe('prov-app App', () => {
  let page: ProvAppPage;

  beforeEach(() => {
    page = new ProvAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
