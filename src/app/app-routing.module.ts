import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './core/layout/main.component';
import { HomeComponent } from './core/pages/home.component';
import { NotfoundComponent } from './core/pages/notfound.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'inventory', loadChildren: 'app/inventory/inventory.module#InventoryModule' },
      { path: '**', component: NotfoundComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
