import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { WareHouse } from '../models';
import { WarehouseService } from '../services';

@Component({
  selector: 'app-modal-search-warehouse',
  templateUrl: './modal-search-warehouse.component.html',
  styles: []
})
export class ModalSearchWarehouseComponent implements OnInit {
  @ViewChild('modal') modalRef: ModalDirective;
  @Output() selected = new EventEmitter<WareHouse>();

  warehouses: WareHouse[] = [];

  constructor(private _warehouseService: WarehouseService) { }

  onSelect(item: WareHouse): void {
    this.selected.emit(item);
    this.hide();
  }

  show(): void {
    this.modalRef.show();
  }

  hide(): void {
    this.modalRef.hide();
  }

  ngOnInit() {
    this._warehouseService.getLists()
      .subscribe(items => this.warehouses = items);
  }

}
