import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { Color } from '../models';
import { ColorService } from '../services';

@Component({
  selector: 'app-modal-search-color',
  templateUrl: './modal-search-color.component.html',
  styles: []
})
export class ModalSearchColorComponent implements OnInit {
  @ViewChild('modal') modalRef: ModalDirective;
  @Output() selected = new EventEmitter<Color>();

  colors: Color[] = [];

  constructor(private _colorService: ColorService) { }

  onSelect(item: Color): void {
    this.selected.emit(item);
    this.hide();
  }

  show(): void {
    this.modalRef.show();
  }

  hide(): void {
    this.modalRef.hide();
  }

  ngOnInit() {
    this._colorService.getLists()
      .subscribe(items => this.colors = items);
  }

}
