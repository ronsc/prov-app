import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { CreditorInfo } from '../models';
import { CreditorService } from '../services';

@Component({
  selector: 'app-modal-search-creditor',
  templateUrl: './modal-search-creditor.component.html',
  styles: [`
    tr {
      cursor: pointer;
    }
  `]
})
export class ModalSearchCreditorComponent implements OnInit {
  @ViewChild('modal') modalRef: ModalDirective;
  @Output() selected = new EventEmitter<CreditorInfo>();

  creditorInfos: CreditorInfo[] = [];

  condition: { creditor: 'NAME' | 'ADDRESS', keyword: string, branch: 'CURRENT' | 'ALL' };

  constructor(private _creditorService: CreditorService) {
    this.condition = { creditor: 'NAME', keyword: '', branch: 'CURRENT' };
  }

  search(): void { }

  onSelect(creditorInfo: CreditorInfo): void {
    this.selected.emit(creditorInfo);
    this.hide();
  }

  show(): void {
    this.modalRef.show();
  }

  hide(): void {
    this.modalRef.hide();
  }

  ngOnInit() {
    this._creditorService.getLists()
      .subscribe(items => this.creditorInfos = items);
  }

}
