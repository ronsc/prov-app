import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { ProductList, ProductInfo } from '../models';
import { ModalSearchProductComponent } from './modal-search-product.component';
import { ModalSearchSnComponent } from './modal-search-sn.component';

@Component({
  selector: 'app-modal-add-product',
  templateUrl: './modal-add-product.component.html',
  styles: [`
    :host {
      user-select: none;
    }
  `]
})
export class ModalAddProductComponent implements OnInit {
  @ViewChild('modal') modalRef: ModalDirective;
  @ViewChild(ModalSearchProductComponent) modalSearchProduct: ModalSearchProductComponent;
  @ViewChild(ModalSearchSnComponent) modalSearchSN: ModalSearchSnComponent;
  @Output() selected = new EventEmitter<ProductList>();

  productList: ProductList;

  constructor() {
    this.clear();
  }

  save(): void {
    this.selected.emit(this.productList);
    this.hide();
    this.clear();
  }

  clear(): void {
    this.productList = new ProductList();
  }

  showSearchProduct(): void {
    this.modalSearchProduct.show();
  }

  showSearchSN(): void {
    this.modalSearchSN.show();
  }

  get total(): number {
    if (isNaN(this.productList.amount) || isNaN(this.productList.price)) {
      return;
    }
    return this.productList.amount * this.productList.price;
  }

  show(): void {
    this.modalRef.show();
  }

  hide(): void {
    this.modalRef.hide();
  }

  private setProduct(item: ProductInfo): void {
    this.productList.name = `${item.group.name} ${item.name}`;
    this.productList.productInfo = item;
    // Auto Set Value
    this.productList.price = item.priceAgent;
  }

  ngOnInit() {
    this.modalSearchProduct.selected
      .subscribe(item => this.setProduct(item));
  }

}
