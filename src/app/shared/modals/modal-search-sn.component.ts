import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { ProductList, SerialNumber, Color, WareHouse } from '../models';
import { ModalSearchColorComponent } from './modal-search-color.component';
import { ModalSearchWarehouseComponent } from './modal-search-warehouse.component';

@Component({
  selector: 'app-modal-search-sn',
  templateUrl: './modal-search-sn.component.html',
  styles: []
})
export class ModalSearchSnComponent implements OnInit {
  @ViewChild('modal') modalRef: ModalDirective;
  @ViewChild(ModalSearchColorComponent) modalSearchColor: ModalSearchColorComponent;
  @ViewChild(ModalSearchWarehouseComponent) modalSearchWH: ModalSearchWarehouseComponent;
  @Input() productList: ProductList;

  serialNumber: SerialNumber;

  constructor() {
    this.clear();
  }

  add(): void {
    this.productList.serialNumber.push(this.serialNumber);
    this.clear();
  }

  save(): void {
    // Set Amount
    this.productList.amount = this.productList.serialNumber.length;
    this.hide();
    this.clear();
  }

  clear(): void {
    this.serialNumber = new SerialNumber();
  }

  showSearchColor(): void {
    this.modalSearchColor.show();
  }

  showSearchWarehouse(): void {
    this.modalSearchWH.show();
  }

  show(): void {
    this.modalRef.show();
  }

  hide(): void {
    this.modalRef.hide();
  }

  ngOnInit() {
    this.modalSearchColor.selected
      .subscribe(item => this.serialNumber.color = item);

    this.modalSearchWH.selected
      .subscribe(item => this.serialNumber.warehouse = item);
  }

}
