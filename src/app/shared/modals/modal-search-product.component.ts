import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { ProductInfo, ProductGroup } from '../models';
import { ProductService } from '../services';

@Component({
  selector: 'app-modal-search-product',
  templateUrl: './modal-search-product.component.html',
  styles: [`
    tr {
      cursor: pointer;
    }
  `]
})
export class ModalSearchProductComponent implements OnInit {
  @ViewChild('modal') modalRef: ModalDirective;
  @Output() selected = new EventEmitter<ProductInfo>();

  productGroupLists: ProductGroup[] = [];
  productInfoLists: ProductInfo[] = [];
  selectedItem: ProductGroup;

  constructor(private _productService: ProductService) { }

  onSelectGroup(item: ProductGroup): void {
    this.selectedItem = item;
    this._productService.findByGroup(item)
      .subscribe(items => this.productInfoLists = items);
  }

  onSelectProduct(item: ProductInfo): void {
    this.selected.emit(item);
    this.hide();
  }

  show(): void {
    this.modalRef.show();
  }

  hide(): void {
    this.modalRef.hide();
  }

  ngOnInit() {
    this._productService.getProductGroupLists()
      .subscribe(items => {
        this.productGroupLists = items;
        // Select First List
        this.onSelectGroup(this.productGroupLists[0]);
      });
  }

}
