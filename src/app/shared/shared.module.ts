import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ModalModule } from 'ngx-bootstrap/modal';

import {
  ModalSearchProductComponent,
  ModalAddProductComponent,
  ModalSearchCreditorComponent,
  ModalSearchSnComponent,
  ModalSearchColorComponent,
  ModalSearchWarehouseComponent
} from './modals';

import {
  ProductService,
  BillService,
  CreditorService,
  ColorService,
  WarehouseService
} from './services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ModalModule.forRoot(),
  ],
  declarations: [
    ModalSearchProductComponent,
    ModalAddProductComponent,
    ModalSearchCreditorComponent,
    ModalSearchSnComponent,
    ModalSearchColorComponent,
    ModalSearchWarehouseComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ModalModule,
    ModalSearchProductComponent,
    ModalAddProductComponent,
    ModalSearchCreditorComponent,
    ModalSearchSnComponent,
    ModalSearchColorComponent,
    ModalSearchWarehouseComponent
  ],
  providers: [
    ProductService,
    BillService,
    CreditorService,
    ColorService,
    WarehouseService
  ]
})
export class SharedModule { }
