import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { ProductInfo } from '../models/product-info';
import { ProductGroup } from '../models/product-group';
import { ProductType } from '../models/product-type';

import { environment } from '../../../environments/environment';

import { Observable } from 'Rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {

  constructor(private _http: Http) { }

  getLists(): Observable<ProductInfo[]> {
    return this._http.get(`${environment.apiUrl}/product-info`)
      .map(response => response.json());
  }

  getProductGroupLists(): Observable<ProductGroup[]> {
    return this._http.get(`${environment.apiUrl}/product-group`)
      .map(response => response.json());
  }

  getProductTypeLists(): Observable<ProductType[]> {
    return this._http.get(`${environment.apiUrl}/product-type`)
      .map(response => response.json());
  }

  findByGroup(item: ProductGroup): Observable<ProductInfo[]> {
    return this._http.get(`${environment.apiUrl}/product-info?group.code=${item.code}`)
      .map(response => response.json());
  }

  create(item: ProductInfo): void {
    // TODO: remove
    item.id = Date.now();
    item.no = `${item.group.code}-${Date.now()}`;

    this._http.post(`${environment.apiUrl}/product-info`, item)
      .subscribe(data => console.log(data));
  }
}
