import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Color } from '../models/color';
import { environment } from '../../../environments/environment';

import { Observable } from 'Rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ColorService {

  constructor(private _http: Http) { }

  getLists(): Observable<Color[]> {
    return this._http.get(`${environment.apiUrl}/color`)
      .map(response => response.json());
  }

}
