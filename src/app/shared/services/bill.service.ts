import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { BillInfo } from '../models/bill-info';
import { environment } from '../../../environments/environment';

import { Observable } from 'Rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class BillService {
  apiUrl = `${environment.apiUrl}/bill-info`;

  constructor(private _http: Http) { }

  getLists(): Observable<BillInfo[]> {
    return this._http.get(this.apiUrl)
      .map(response => response.json());
  }

  create(item: BillInfo): void {
    this._http.post(this.apiUrl, item)
      .subscribe((data) => console.log(data));
  }

}
