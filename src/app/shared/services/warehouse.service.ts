import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { WareHouse } from '../models/ware-house';
import { environment } from '../../../environments/environment';

import { Observable } from 'Rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class WarehouseService {

  constructor(private _http: Http) { }

  getLists(): Observable<WareHouse[]> {
    return this._http.get(`${environment.apiUrl}/warehouse`)
      .map(response => response.json());
  }

}
