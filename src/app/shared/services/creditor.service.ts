import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { CreditorInfo } from '../models/creditor-info';
import { environment } from '../../../environments/environment';

import { Observable } from 'Rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class CreditorService {
  apiUrl = `${environment.apiUrl}/creditor-info`;

  constructor(private _http: Http) { }

  getLists(): Observable<CreditorInfo[]> {
    return this._http.get(this.apiUrl)
      .map(response => response.json());
  }

  create(item: CreditorInfo): void {
    // TODO: remove
    item.id = Date.now();
    item.no = `LT-${Date.now()}`;

    this._http.post(this.apiUrl, item)
      .subscribe(data => console.log(data));
  }
}
