import { ProductGroup, PRODUCTGROUPS } from './product-group';
import { ProductType, PRODUCTTYPES } from './product-type';

export class ProductInfo {
    public id: number;
    public no: string;
    public name: string;
    public unit: string;
    public group: ProductGroup;
    public type: ProductType;
    public min: number;
    public detail: string;
    public priceAgent: number;
    public priceCash: number;
    public priceInstallment: number;

    constructor() {
        this.min = 1;
        this.group = new ProductGroup();
        this.type = new ProductType();
    }
}

export const PRODUCTINFOS: ProductInfo[] = [
    {
        id: 1,
        no: `${PRODUCTGROUPS[0].code}-0007`,
        name: 'BN135-G',
        unit: PRODUCTGROUPS[0].unit,
        group: PRODUCTGROUPS[0],
        type: PRODUCTTYPES[0],
        min: 1,
        detail: 'สตาร์ทมือ',
        priceAgent: 40000,
        priceCash: 43000,
        priceInstallment: 45000,
    },
    {
        id: 2,
        no: `${PRODUCTGROUPS[0].code}-0002`,
        name: 'BN600I',
        unit: PRODUCTGROUPS[0].unit,
        group: PRODUCTGROUPS[0],
        type: PRODUCTTYPES[0],
        min: 1,
        detail: 'Standard',
        priceAgent: 30000,
        priceCash: 33000,
        priceInstallment: 35000,
    },
    {
        id: 3,
        no: `${PRODUCTGROUPS[1].code}-0168`,
        name: 'WW150H(TH)',
        unit: PRODUCTGROUPS[1].unit,
        group: PRODUCTGROUPS[1],
        type: PRODUCTTYPES[0],
        min: 1,
        detail: '',
        priceAgent: 60000,
        priceCash: 65000,
        priceInstallment: 68000,
    },
    {
        id: 4,
        no: `${PRODUCTGROUPS[1].code}-0157`,
        name: 'NBC110MDFH(TH)',
        unit: PRODUCTGROUPS[1].unit,
        group: PRODUCTGROUPS[1],
        type: PRODUCTTYPES[0],
        min: 1,
        detail: '',
        priceAgent: 37000,
        priceCash: 39000,
        priceInstallment: 40000,
    }
];
