import { Color } from './color';
import { WareHouse } from './ware-house';

export class SerialNumber {
    public id: number;
    public engineNumber: string;
    public bodyNumber: string;
    public color: Color;
    public regNumber: string;
    public keyNumber: string;
    public warehouse: WareHouse;
}
