export class CreditorInfo {
    public id: number;
    public no: string;
    public title: string;
    public name: string;
    public lastName: string;
    public surName: string;
    public address: string;
    public tumbol: string;
    public amphoe: string;
    public changwat: string;
    public postCode: number;
    public tel: string;
    public fax: string;
    public site: string;
    public email: string;
    public contact: string;
    public note: string;
    public transport: ('NONE' | 'DELIVER' | 'EMS') = 'NONE';
    public paymentType: ('CASH' | 'CREDIT') = 'CASH';
    public creditDay = 0;
    public vatType: ('INCLUSIVE' | 'EXCLUSIVE' | 'NONE') = 'INCLUSIVE';
}
