import { CreditorInfo } from './creditor-info';
import { ProductList } from './product-list';

export class BillInfo {
    public id: number;
    public billNo: string;
    public billDate: string;
    public taxNo: string;
    public creditorNo: string;
    public creditorName: string;
    public creditorAddress: string;
    public creditorTel: string;
    public paymentType: ('CREDIT' | 'CASH') = 'CASH';
    public creditDay = 0;
    public productLists: ProductList[] = [];
    public vatType: ('INCLUSIVE' | 'EXCLUSIVE' | 'NONE') = 'INCLUSIVE';
    public billNote: string;
}
