export class ProductType {
    public id: number;
    public code: string;
    public name: string;
}

export const PRODUCTTYPES: ProductType[] = [
    {
        id: 1,
        code: 'MC',
        name: 'รถจักรยานยนต์'
    },
    {
        id: 2,
        code: 'OT',
        name: 'Other'
    }
];
