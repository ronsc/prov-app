export class Color {
    public id: number;
    public no: string;
    public name: string;
}

export const COLORS: Color[] = [
    { id: 1, no: 'CO-004', name: 'ขาว' },
    { id: 2, no: 'CO-057', name: 'ขาว-เขียว' },
    { id: 3, no: 'CO-090', name: 'ขาว-เขียว-ดำ' },
    { id: 4, no: 'CO-069', name: 'ขาว-ชมพู' },
    { id: 5, no: 'CO-013', name: 'ขาว-ชมพู-น้ำเงิน' },
];
