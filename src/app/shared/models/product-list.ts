import { ProductInfo } from './product-info';
import { SerialNumber } from './serial-number';

export class ProductList {
    public id: number;
    public productInfo: ProductInfo;
    public name: string;
    public amount: number;
    public price: number;
    public serialNumber: SerialNumber[] = [];
}
