export class WareHouse {
    public id: number;
    public no: string;
    public name: string;
}

export const WAREHOUSES: WareHouse[] = [
    { id: 1, no: 'ST-001', name: 'หน้าร้าน' },
    { id: 2, no: 'ST-002', name: 'ACB125CBTF(TH)' },
];
