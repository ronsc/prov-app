export class ProductGroup {
    public id: number;
    public code: string;
    public name: string;
    public unit: string;
}

export const PRODUCTGROUPS: ProductGroup[] = [
    {
        id: 1,
        code: 'BL',
        name: 'Benelli',
        unit: 'คัน',
    },
    {
        id: 2,
        code: 'HD',
        name: 'HONDA',
        unit: 'คัน',
    },
    {
        id: 3,
        code: 'YA',
        name: 'YAMAHA',
        unit: 'คัน',
    }
];
