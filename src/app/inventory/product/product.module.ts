import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ProductComponent } from './product.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [ProductComponent]
})
export class ProductModule { }
