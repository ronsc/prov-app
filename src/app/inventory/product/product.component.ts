import { Component, OnInit } from '@angular/core';

import { ProductInfo, ProductGroup, ProductType, ProductService } from '../../shared';

import { Observable } from 'Rxjs/Observable';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styles: []
})
export class ProductComponent implements OnInit {
  productInfo: ProductInfo;
  productGroupLists: ProductGroup[];
  productTypeLists: ProductType[];

  constructor(private _productService: ProductService) {
    this.productInfo = new ProductInfo();
  }

  save(): void {
    this._productService.create(this.productInfo);
    this.clear();
  }

  clear(): void {
    this.productInfo = new ProductInfo();
  }

  setProductGroup(item: ProductGroup): void {
    this.productInfo.group = item;
    this.productInfo.unit = item.unit;
  }

  setProductType(item: ProductType): void {
    this.productInfo.type = item;
  }

  ngOnInit() {
    this._productService.getProductGroupLists()
      .subscribe((items) => this.productGroupLists = items);

    this._productService.getProductTypeLists()
      .subscribe((items) => this.productTypeLists = items);
  }

}
