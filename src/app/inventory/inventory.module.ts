import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { SharedModule } from '../shared/shared.module';

import { InventoryRoutingModule } from './inventory-routing.module';
import { BuyModule } from './buy/buy.module';
import { BuyReportModule } from './buy-report/buy-report.module';
import { ProductModule } from './product/product.module';
import { CreditorModule } from './creditor/creditor.module';

@NgModule({
  imports: [
    InventoryRoutingModule,
    BuyModule,
    BuyReportModule,
    ProductModule,
    CreditorModule,
  ]
})
export class InventoryModule { }
