import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { BuyReportComponent } from './buy-report.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [BuyReportComponent]
})
export class BuyReportModule { }
