import { Component, OnInit } from '@angular/core';

import { BillInfo, BillService } from '../../shared';

@Component({
  selector: 'app-buy-report',
  templateUrl: './buy-report.component.html',
  styles: []
})
export class BuyReportComponent implements OnInit {
  billInfos: BillInfo[] = [];

  constructor(private _billService: BillService) { }

  ngOnInit() {
    this._billService.getLists()
      .subscribe(items => this.billInfos);
  }

}
