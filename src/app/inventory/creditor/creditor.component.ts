import { Component, OnInit } from '@angular/core';

import { CreditorInfo, CreditorService } from '../../shared';

@Component({
  selector: 'app-creditor',
  templateUrl: './creditor.component.html',
  styles: []
})
export class CreditorComponent implements OnInit {
  creditorInfo: CreditorInfo;

  constructor(private _creditorService: CreditorService) {
    this.clear();
  }

  save(): void {
    this._creditorService.create(this.creditorInfo);
    this.clear();
  }

  clear(): void {
    this.creditorInfo = new CreditorInfo();
  }

  ngOnInit() {
  }

}
