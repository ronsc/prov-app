import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { CreditorComponent } from './creditor.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [CreditorComponent]
})
export class CreditorModule { }
