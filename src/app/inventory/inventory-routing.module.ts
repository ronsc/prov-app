import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyComponent } from './buy/buy.component';
import { CreditorComponent } from './creditor/creditor.component';
import { BuyReportComponent } from './buy-report/buy-report.component';
import { ProductComponent } from './product/product.component';

const routes: Routes = [
  { path: 'M1', component: BuyComponent },
  { path: 'R1', component: BuyReportComponent },
  { path: 'S1', component: ProductComponent },
  { path: 'S2', component: CreditorComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
