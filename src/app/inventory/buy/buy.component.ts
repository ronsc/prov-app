import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { ModalDirective } from 'ngx-bootstrap/modal';

import {
  ModalSearchCreditorComponent,
  ModalAddProductComponent,
  ModalSearchSnComponent,
  BillInfo,
  ProductList,
  BillService,
  CreditorInfo
} from '../../shared';

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styles: []
})
export class BuyComponent implements OnInit {
  @ViewChild(ModalSearchCreditorComponent) modalSearchCreditor: ModalSearchCreditorComponent;
  @ViewChild(ModalAddProductComponent) modalAddProduct: ModalAddProductComponent;
  @ViewChild(ModalSearchSnComponent) modalSearchSN: ModalSearchSnComponent;

  today: Date;
  billInfo: BillInfo;
  productListSelect: ProductList = new ProductList();

  constructor(private _router: Router, private _billService: BillService) {
    this.today = new Date();
    this.billInfo = new BillInfo();

    this.today.setFullYear(this.today.getFullYear() + 543);
  }

  newCreditor(): void {
    this._router.navigate(['/inventory/S2']);
  }

  save(): void {
    this._billService.create(this.billInfo);
  }

  clear(): void {
    this.billInfo = new BillInfo();
  }

  showSearchCreditor(): void {
    this.modalSearchCreditor.show();
  }

  showAddProduct(): void {
    this.modalAddProduct.show();
  }

  showSearchSN(item): void {
    this.productListSelect = item;
    this.modalSearchSN.show();
  }

  private setCreditor(creditor: CreditorInfo): void {
    this.billInfo.creditorNo = creditor.no;
    this.billInfo.creditorName = `${creditor.title}${creditor.name} ${creditor.lastName}`;
    this.billInfo.creditorAddress = `${creditor.address} ${creditor.tumbol} ${creditor.amphoe} ${creditor.changwat} ${creditor.postCode}`;
    this.billInfo.creditorTel = creditor.tel;
    this.billInfo.paymentType = creditor.paymentType;
    this.billInfo.creditDay = creditor.creditDay;
    this.billInfo.vatType = creditor.vatType;
  }

  private setProductList(item: ProductList): void {
    if (item.serialNumber.length === 0) {
      return;
    }
    this.billInfo.productLists.push(item);
  }

  ngOnInit() {
    this.modalSearchCreditor.selected
      .subscribe(item => this.setCreditor(item));

    this.modalAddProduct.selected
      .subscribe(item => this.setProductList(item));
  }

}
