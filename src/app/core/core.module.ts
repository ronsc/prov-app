import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from './navbar.component';
import { MainComponent } from './layout/main.component';
import { ReportComponent } from './layout/report.component';
import { HomeComponent } from './pages/home.component';
import { NotfoundComponent } from './pages/notfound.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot()
  ],
  declarations: [HomeComponent, NavbarComponent, MainComponent, ReportComponent, NotfoundComponent],
  exports: [HomeComponent, ReportComponent, MainComponent, NotfoundComponent]
})
export class CoreModule { }
